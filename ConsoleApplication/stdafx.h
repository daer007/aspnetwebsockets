// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <cpprest/streams.h>
#include <cpprest/containerstream.h>
#include <cpprest/ws_client.h>
#include <cpprest/http_client.h>
#include <cpprest/uri.h>
#include <cpprest/json.h>
#include <cpprest/asyncrt_utils.h>
#include <cpprest/ws_msg.h>


// TODO: reference additional headers your program requires here
