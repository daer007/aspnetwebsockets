// ConsoleApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

using namespace utility;
using namespace utility::conversions;
using namespace web;
using namespace web::web_sockets::client;

int _tmain(int argc, _TCHAR* argv[])
{
	websocket_callback_client client;
	client.connect(U("ws://10.0.1.82:65284/api/websocket")).wait();

	client.set_message_handler([](websocket_incoming_message msg)
	{
		msg.extract_string().then([](std::string body) {
			std::cout << body << std::endl;
		});
	});

	while (true) {
		std::string value;
		std::cin >> value;
	}
	return 0;
}

