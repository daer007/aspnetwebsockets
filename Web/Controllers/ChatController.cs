﻿using Microsoft.Web.WebSockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Web.Controllers
{
    public class ChatController : ApiController
    {
        public HttpResponseMessage Get(string username) {
            HttpContext.Current.AcceptWebSocketRequest(new ChatWebSocketHandler(username));
            return Request.CreateResponse(HttpStatusCode.SwitchingProtocols);
        }

        public class ChatWebSocketHandler : WebSocketHandler
        {
            private static WebSocketCollection _clients = new WebSocketCollection();
            private string _username;

            public ChatWebSocketHandler(string username)
            {
                this._username = username;
            }

            public override void OnOpen()
            {
                _clients.Add(this);
            }

            public override void OnClose()
            {
                _clients.Remove(this);
            }

            public override void OnMessage(string message)
            {
                _clients.Broadcast(_username + ": " + message);
            }




            public static void SendMessage(string message) 
            {
                _clients.Broadcast(message);
            }
        }
    }
}
